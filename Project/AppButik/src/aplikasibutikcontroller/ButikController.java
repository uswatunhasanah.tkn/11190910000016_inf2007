package aplikasibutikcontroller;

import aplikasibutikmodel.Butik;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import static java.lang.System.in;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java.util.Scanner;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author User
 */
public class ButikController {

    private static final String FILE = "G:\\butik.json";
    private Butik butik;
    private int NoTransaksi;
    private int ModelBaju;
    private int JumlahBaju;
    private String WarnaBaju;
    private int LamaPenyewaan;
    private BigDecimal Biaya;
    private int Pilihan;
    private boolean keluar = false;
    private final Scanner in;

    public ButikController() {
        in = new Scanner(System.in);
    }

    public void setPenyewaanBaju() {
        System.out.println("Nomor Transaksi : ");
        NoTransaksi= in.nextInt();
        System.out.println("Model Baju 1=Kebaya, 2=Gaun Pernikahan, 3=Gaun Ulang Tahun");
        System.out.print("Masukkan Model Baju : ");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.out.printf("\"%s\" is not a valid number.\n", input);
            System.out.print("Masukkan Model Baju : ");
        }

        ModelBaju = in.nextInt();
        System.out.println("Masukan Jumlah Baju");
        JumlahBaju = in.nextInt();
        System.out.println("Masukkan Warna Baju");
        WarnaBaju = in.next();
        System.out.println("Masukkan Lama Penyewaan");
        LamaPenyewaan = in.nextInt();

        butik = new Butik();
        butik.setNoTransaksi(NoTransaksi);
        butik.setModelBaju(ModelBaju);
        butik.setJumlahBaju(JumlahBaju);
        butik.setWarnaBaju(WarnaBaju);
        butik.setLamaPenyewaan(LamaPenyewaan);

        setWriteButik(FILE, butik);

        System.out.println("Apakah mau Input kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        Pilihan = in.nextInt();
        if (Pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setPenyewaanBaju();
        }
    }

    public void setWriteButik(String file, Butik butik) {
        Gson gson = new Gson();

        List<Butik> butiks = getReadButik(file);
        butiks.remove(butik);
        butiks.add(butik);

        String json = gson.toJson(butiks);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(ButikController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setPengembalianBaju() {
        System.out.print("Masukkan Nomor Transaksi: ");
        NoTransaksi = in.nextInt();

        Butik p = getSearch(NoTransaksi);
        if (p != null) {

            if (p.getLamaPenyewaan() > 0) {
                Biaya = new BigDecimal(p.getLamaPenyewaan());
                if (p.getModelBaju() == 1) {
                    Biaya = Biaya.multiply(new BigDecimal(150000));
                } else if (p.getModelBaju() == 2) {
                    Biaya = Biaya.multiply(new BigDecimal(250000));
                } else {
                    Biaya = Biaya.multiply(new BigDecimal(350000));
                }

                p.setBiaya(Biaya);
                p.setKeluar(true);
            }
            if (p.getModelBaju() == 1) {
                System.out.println("Model Baju : Kebaya");
            } else if (p.getModelBaju() == 2) {
                System.out.println("Model Baju : Gaun Pernikahan");
            } else {
                System.out.println("Model Baju : Gaun Ulang Tahun");
            }
            System.out.println("Jumlah Baju : " + p.getJumlahBaju());
            System.out.println("Warna Baju : " + p.getWarnaBaju());
            System.out.println("Lama Penyewaan : " + p.getLamaPenyewaan());
            System.out.println("Total Harga : " + Biaya);
            
            System.out.println("Proses Bayar?");
            System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
            Pilihan = in.nextInt();
            switch (Pilihan) {
                case 1:
                    setWriteButik(FILE, p);
                    break;
                case 2:
                    setPengembalianBaju();
                    break;
                default:
                    Menu m = new Menu();
                    m.getMenuAwal();
                    break;
            }
            System.out.println("Apakah mau memproses kembali?");
            System.out.print("1) Ya, 2) Tidak : ");
            Pilihan = in.nextInt();
            if (Pilihan == 2) {
                Menu m = new Menu();
                m.getMenuAwal();
            } else {
                setPengembalianBaju();
            }
        } else {
            System.out.println("Data tidak ditemukan");
            setPengembalianBaju();
        }

    }

    public Butik getSearch(int NoTransaksi) {
        List<Butik> butiks = getReadButik(FILE);

        Butik p = butiks.stream()
                .filter(pp -> NoTransaksi == pp.getNoTransaksi())
                .findAny()
                .orElse(null);

        return p;

    }

    public List<Butik> getReadButik(String file) {
        List<Butik> butiks = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                Butik[] ps = gson.fromJson(line, Butik[].class);
                butiks.addAll(Arrays.asList(ps));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ButikController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ButikController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return butiks;
    }

    public void getDataButik() {
        List<Butik> butiks = getReadButik(FILE);
        Predicate<Butik> isKeluar = e -> e.isKeluar() == true;

        List<Butik> pResults = butiks.stream().filter(isKeluar).collect(Collectors.toList());
        BigDecimal total = pResults.stream()
                .map(Butik::getBiaya)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("NoTransaksi\tModel Baju \tJumlah Baju \t\tWarna Baju\t\tBiaya");
        System.out.println("-----------\t---------- \t----------- \t\t----------\t\t-----");
        pResults.forEach((p) -> {
           if (p.getModelBaju()==1) {
            System.out.println(p.getNoTransaksi() + "\t\t" +"Kebaya" + "\t\t" + p.getJumlahBaju() + "\t" + p.getWarnaBaju() + "\t" + p.getBiaya());
           } else if (p.getModelBaju()==2) {
            System.out.println(p.getNoTransaksi() + "\t\t" +"Gaun Pernikahan" + "\t\t" + p.getJumlahBaju() + "\t" + p.getWarnaBaju() + "\t" + p.getBiaya());
   
           } else {
               System.out.println(p.getNoTransaksi() + "\t\t" +"Gaun Ulang Tahun" + "\t\t" + p.getJumlahBaju() + "\t" + p.getWarnaBaju() + "\t" + p.getBiaya());

           }
        
           });
        System.out.println("-----------\t---------- \t----------- \t\t----------\t\t-----");
        System.out.println("====================================");
        System.out.println("Pendapatan Total = Rp. " + total);
        System.out.println("====================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        Pilihan = in.nextInt();
        if (Pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataButik();
        }
    }

}
