package aplikasibutikcontroller;

import aplikasibutikmodel.Info;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class Menu {

    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("-------------------------------------------------");

        System.out.println("DAFTAR MENU");
        System.out.println("1. Menu Penyewaan Baju");
        System.out.println("2. Menu Pengembalian Baju");
        System.out.println("3. Laporan Harian");
        System.out.println("4. Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3/4) : ");

        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3/4) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }

    public void setPilihMenu() {
        ButikController pc = new ButikController();
        switch (noMenu) {
            case 1:
                pc.setPenyewaanBaju();
                break;
            case 2:
                pc.setPengembalianBaju();
                break;
            case 3:
                pc.getDataButik();
                break;
            case 4:
                System.out.println("Sampai jumpa :)");
                System.exit(0);
                break;
        }
    }
}

