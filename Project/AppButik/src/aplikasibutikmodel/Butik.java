package aplikasibutikmodel;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author User
 */
public class Butik implements Serializable {

    private static final long serialVersionUID = -6756463875294313469L;
    private int NoTransaksi;
    private int ModelBaju;
    private int JumlahBaju;
    private String WarnaBaju;
    private int LamaPenyewaan;
    private BigDecimal Biaya;
    private boolean keluar = false;

    public Butik() {

    }

    public Butik(int NoTransaksi, int ModelBaju, int JumlahBaju, String WarnaBaju, int LamaPenyewaan, BigDecimal Biaya) {
        this.NoTransaksi = NoTransaksi;
        this.ModelBaju = ModelBaju;
        this.JumlahBaju = JumlahBaju;
        this.WarnaBaju = WarnaBaju;
        this.LamaPenyewaan = LamaPenyewaan;
        this.Biaya = Biaya;
    }

    public int getNoTransaksi() {
        return NoTransaksi;
    }

    public void setNoTransaksi(int NoTransaksi) {
        this.NoTransaksi = NoTransaksi;
    }

    public int getModelBaju() {
        return ModelBaju;
    }

    public void setModelBaju(int ModelBaju) {
        this.ModelBaju = ModelBaju;
    }

    public int getJumlahBaju() {
        return JumlahBaju;
    }

    public void setJumlahBaju(int JumlahBaju) {
        this.JumlahBaju = JumlahBaju;
    }

    public String getWarnaBaju() {
        return WarnaBaju;
    }

    public void setWarnaBaju(String WarnaBaju) {
        this.WarnaBaju = WarnaBaju;
    }

    public int getLamaPenyewaan() {
        return LamaPenyewaan;
    }

    public void setLamaPenyewaan(int LamaPenyewaan) {
        this.LamaPenyewaan = LamaPenyewaan;
    }

    public BigDecimal getBiaya() {
        return Biaya;
    }

    public void setBiaya(BigDecimal Biaya) {
        this.Biaya = Biaya;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }

    @Override
    public String toString() {
        return "Butik{" + "NoTransaksi=" + NoTransaksi + ", ModelBaju=" + ModelBaju + ", JumlahBaju=" + JumlahBaju + ", WarnaBaju=" + WarnaBaju + ", LamaPenyewaan=" + LamaPenyewaan + ", Biaya=" + Biaya + ", keluar=" + keluar + '}';
    }

}
