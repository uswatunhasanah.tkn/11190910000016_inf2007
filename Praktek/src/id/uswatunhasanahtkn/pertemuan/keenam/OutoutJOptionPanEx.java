package id.uswatunhasanahtkn.pertemuan.keenam;

import javax.swing.JOptionPane;
/**
 *
 * @author User
 */
public class OutoutJOptionPanEx {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukkan Bilangan");
        
        bilangan = Integer.parseInt(box);
        
        JOptionPane.showMessageDialog(null, "Bilangan: " + bilangan, "Hasil input", JOptionPane.INFORMATION_MESSAGE);
    }
}
