package id.uswatunhasanahtkn.pertemuan.keenam;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class InputScannerEX {
    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan bilangan");
        bilangan = in.nextInt();
        
        System.out.println("Bilangan: " + bilangan);
    }
}
