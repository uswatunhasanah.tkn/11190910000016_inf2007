package id.uswatunhasanahtkn.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Faktorial {
    public static void main(String[] args) {
        int n, fak, i;
        
        Scanner in = new Scanner(System.in);
        System.out.println("n = ");
        n = in.nextInt();
        fak = 1;
        
        for (i = 1; i <= n; i++) {
            fak = fak * i;
        }
        System.out.println("hasil = " + fak);
    }
}
