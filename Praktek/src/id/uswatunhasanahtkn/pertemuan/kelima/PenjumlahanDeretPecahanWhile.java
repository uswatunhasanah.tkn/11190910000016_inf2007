package id.uswatunhasanahtkn.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class PenjumlahanDeretPecahanWhile {
     public static void main(String[] args) {
        int x;
        float s = 0;
        
        Scanner in = new Scanner(System.in);
        System.out.println("x = ");
        x = in.nextInt();
        
        while ( x != -1) {
            s = s + (float) 1 /x;
            System.out.println("x = ");
            x = in.nextInt();
        }
        System.out.println("jumlah = " + s);
    }
}
