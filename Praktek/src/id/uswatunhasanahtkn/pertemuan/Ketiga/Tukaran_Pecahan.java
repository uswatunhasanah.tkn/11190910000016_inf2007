package id.uswatunhasanah.tkn.pertemuan.ketiga;

/**
 *
 * @author HP
 */
   
import java.util.Scanner;

public class Tukaran_Pecahan {
    public static void main(String[] args) {
        int JumlahUang, pecahan1, pecahan2, pecahan3, pecahan4,pecahan5, sisa;
        Scanner in = new Scanner(System.in);
        System.out.println("jumlah uang : ");
        JumlahUang = in.nextInt ();
        
        pecahan1 = JumlahUang / 1000;
        sisa = JumlahUang % 1000;
        pecahan2 = sisa / 500;
        sisa = sisa % 500;
        pecahan3 = sisa / 100;
        sisa = sisa % 100;
        pecahan4 = sisa / 50;
        sisa = sisa % 50;
        pecahan5 = sisa / 25;
        
        System.out.println("tukaran pecahan");
        System.out.println("Rp. 1000 = " + pecahan1 + " buah ");
        System.out.println("Rp. 500 = " + pecahan2 + " buah ");
        System.out.println("Rp. 100 = " + pecahan3 + " buah ");
        System.out.println("Rp. 50 = " + pecahan4 + " buah ");
        System.out.println("Rp. 25 = " + pecahan5 + " buah ");
        
    }
}
