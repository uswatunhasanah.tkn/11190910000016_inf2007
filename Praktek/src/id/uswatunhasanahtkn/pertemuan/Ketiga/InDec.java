package id.uswatunhasanah.tkn.pertemuan.ketiga;

/**
 *
 * @author HP
 */
public class InDec {
    public static void main(String[] args) {
        int x = 8, y = 13;
        System.out.println(" x = " + x);
        System.out.println(" y = " + y);
        System.out.println(" x = " + ++x);
        System.out.println(" y = " + y++);
        System.out.println(" x = " + x--);
        System.out.println(" y = " + --y);
    }
}
