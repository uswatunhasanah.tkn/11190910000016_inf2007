package id.uswatunhasanah.tkn.pertemuan.ketiga;

/**
 *
 * @author HP
 */
import java.util.Scanner;
        
public class Selisih_Dua_Tanggal {
    public static void main(String[] args) {
        int tanggal1, bulan1, tahun1, tanggal2, bulan2, tahun2, tanggal3, bulan3, tahun3, selisih;
        Scanner in = new Scanner (System.in);
        System.out.println("tanggal 1 : ");
        tanggal1 = in.nextInt();
        System.out.println("bulan 1 : ");
        bulan1 = in.nextInt();
        System.out.println("tahun 1 : ");        
        tahun1 = in.nextInt();
        System.out.println("tanggal 2 : ");
        tanggal2 = in.nextInt();
        System.out.println("bulan 2 : ");
        bulan2 = in.nextInt();
        System.out.println("tahun 2 : ");
        tahun2 = in.nextInt();
        
        selisih = (tahun2 - tahun1) *  365 + (bulan2 - bulan1) * 30 + (tanggal2 - tanggal1);
        tahun3 = selisih / 365;
        bulan3 = (selisih % 365) / 30;
        tanggal3 = (selisih * 365)% 30;
        
        System.out.println("selisih");
        System.out.println(tahun3 + " tahun " + bulan3 + "bulan" + tanggal3 + "hari");
    }
}
