package id.uswatunhasanahtkn.pertemuan.keduabelas;

/**
 *
 * @author User
 */
public class Pegawai {
    
    protected String nama;
    protected int gaji;

    public Pegawai(String nama, int gaji) {
        this.nama = nama;
        this.gaji = gaji;
    }

    public int infogaji() {
        return this.gaji;
    }
}
