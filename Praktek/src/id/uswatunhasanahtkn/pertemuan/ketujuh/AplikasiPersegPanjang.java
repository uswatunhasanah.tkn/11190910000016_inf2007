package id.uswatunhasanahtkn.pertemuan.ketujuh;

import java.util.Scanner;
/**
 *
 * @author User
 */
public class AplikasiPersegPanjang {
    public static void main(String[] args) {
        // mencoba konstuktor default
        PersegiPanjang persegiPanjang2 = new PersegiPanjang();
        
        Scanner in = new Scanner(System.in);
        double panjang, lebar;
        
        System.out.println("Masukkan Panjang");
        panjang = in.nextDouble();
        
        System.out.println("Masukkan Lebar");
        lebar = in.nextDouble();
        
        PersegiPanjang persegiPanjang = new PersegiPanjang(panjang, lebar);
        persegiPanjang.getInfo();
        System.out.println("Luas : " + persegiPanjang.getLuas());
        System.out.println("Keliling : " + persegiPanjang.getKeliling());
    }
    
}
