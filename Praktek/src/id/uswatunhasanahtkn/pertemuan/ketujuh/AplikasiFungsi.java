package id.uswatunhasanahtkn.pertemuan.ketujuh;

/**
 *
 * @author User
 */
public class AplikasiFungsi {
    public static void main(String[] args) {
        float x = 0;
        Fungsi fungsi = new Fungsi();
        
        System.out.println("----------");
        System.out.println(" x f(x) ");
        System.out.println("----------");
        x = (float) 10.0;
        while (x <= 15.0) {
            System.out.println(x + " " + fungsi.getFungsi(x));
            x = (float) (x + 0.2);
            
        }
        System.out.println("----------");
        }
}
