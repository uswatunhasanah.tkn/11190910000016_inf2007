package id.uswatunhasanahtkn.pertemuan.ketujuh;

/**
 *
 * @author User
 */
public class PersegiPanjang {
    private double panjang;
    private double lebar;
    
    public PersegiPanjang(){
        System.out.println("Konstruktor Persegi Panjang");
    }
    public PersegiPanjang(double panjang, double lebar) {
        this.panjang = panjang;
        this.lebar = lebar;
    }
    public double getLuas(){
        return panjang * lebar;
    }
    public double getKeliling() {
        return 2 * (panjang + lebar);
    }
    public void getInfo() {
        System.out.println("Kelas Persegi Panjang");
    }
}
