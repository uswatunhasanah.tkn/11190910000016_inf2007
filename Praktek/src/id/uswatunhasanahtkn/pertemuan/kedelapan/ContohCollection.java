package id.uswatunhasanahtkn.pertemuan.kedelapan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author User
 */
public class ContohCollection {

    public static void main(String[] args) {
        System.out.println("---List---");
        List namaList = new ArrayList();
        namaList.add(1);
        namaList.add(1);
        namaList.add(2);
        for (int i = 0; i < namaList.size(); i++) {
            System.out.println(namaList.get(i));
        }
        System.out.println("---Set---");
        Set namaSet = new HashSet();
        namaList.add(1);
        namaList.add(1);
        namaList.add(2);
        for (Object o : namaSet) {
            System.out.println(o.toString());
        }
        System.out.println("---Map---");
        Map namaMap = new HashMap();
        namaMap.put(0, "1");
        namaMap.put(1, "2");
        namaMap.put(2, "2");

        namaMap.forEach((k, v) -> {
            System.out.println("Key : " + k + " Value: " + v);
        });

    }
}
