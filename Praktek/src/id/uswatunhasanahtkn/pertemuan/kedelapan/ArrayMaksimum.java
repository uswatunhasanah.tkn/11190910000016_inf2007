package id.uswatunhasanahtkn.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class ArrayMaksimum {
    int i, maks;
    
    public int getMaks(int A[], int n) {
        maks = -9999;
        for (i = 0; i < n; i++) {
            if (A[i] > maks) {
                maks = A[i];
            } 
        }
        return maks;
    }
    public static void main(String[] args) {
        
        int i, n;
        
        ArrayMaksimum arrayMaksimum = new ArrayMaksimum();
        Scanner in = new Scanner(System.in);
        
        System.out.println("N = ");
        n = in.nextInt();
        
        int A[] = new int[n];
        
        for (i = 0; i < A.length; i++) {
            System.out.println("masukkan array [" + i + "] : ");
            A[i] = in.nextInt();
        }
        System.out.println("elemen terbesar = " + arrayMaksimum.getMaks(A, n));
    }
}
