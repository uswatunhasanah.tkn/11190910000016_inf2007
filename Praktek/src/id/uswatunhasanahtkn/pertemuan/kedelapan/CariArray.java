package id.uswatunhasanahtkn.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class CariArray {
    
    int i, j = -1;
    
    public int getCari(int nilai[], int angka) {
        System.out.println("nilai pada array");
        for (i = 0; i < nilai.length; i++) {
            System.out.println(nilai [i] + " ");
        }
        
        System.out.println("");
        for (i = 0; i < nilai.length; i++) {
            if (angka == nilai[i]) {
                 j = i;
                 System.out.println("angka yang dicari (" +  angka + ")");
                 System.out.println(" berada pada indeks ke: " + j);
                 
            }  
        }
        if (j == -1) {   
            System.out.println("0");
        }
        return j;
    }
    
    public static void main(String[] args) {
        int x, n, i, j;
        Scanner in = new Scanner(System.in);
        CariArray app = new CariArray();
        
        System.out.println("N = ");
        n = in.nextInt();
        int[] nilai = new int[n];
        
        for (i = 0; i < nilai.length; i++) {
            System.out.println("masukkan array [" + i + "] : ");
            nilai[i] = in.nextInt();
        }
        System.out.println("masukan angka yang dicari: ");
        x = in.nextInt();
        
        app.getCari(nilai, x);
    }
}
