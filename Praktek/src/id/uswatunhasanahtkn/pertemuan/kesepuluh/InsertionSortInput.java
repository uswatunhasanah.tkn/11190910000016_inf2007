package id.uswatunhasanahtkn.pertemuan.kesepuluh;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class InsertionSortInput {
    int[] getInsertionSortInput (int[] L) {
        int i = 0, j, k, l, y;
        boolean ketemu;
        
        Scanner in = new Scanner(System.in);
        
        while (L[i] != -1) {
            if (i == 0) {
                for (l = 0; l <= i; l++) {
                    System.out.print(L[l]);
                    System.out.println(" ]");
                }
                i++;
                System.out.print("\nmasukkan Array [" + i + "] : ");
                L[i] = in.nextInt();
            }
            
            for (j = i; j > 0; j--) {
                y = L[j];
                k = j - 1;
                ketemu = false;
                while ((k >= 0) && (!ketemu)) {
                    if (y < L[k]) {
                        L[k + 1] = L[k];
                        k = k - 1;
                    } else {
                        ketemu = true;
                    }
                }
                L[k + 1] = y;
            }
            
            System.out.println("Hasil Pengurutan Array");
            System.out.print("[  ");
            for (l = 0; l <= i; l++) {
                    System.out.print(L[l] + "  ");
            }
            
            System.out.println("]");
            i++;
            System.out.print("\nmasukkan Array [" + i + "] : ");
            L[i] = in.nextInt();
        }
        return L;
    }

    public static void main(String[] args) {
        int[] L = new int[100];
        int i = 0;

        Scanner in = new Scanner(System.in);
        InsertionSortInput app = new InsertionSortInput();
        
        System.out.print("masukkan Array [" + i + "] : ");
        L[i] = in.nextInt();
        System.out.println("Hasil pengurutan Array");
        System.out.print("[ ");
        
        app.getInsertionSortInput(L);
    }
}
