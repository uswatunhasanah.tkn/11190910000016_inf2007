package id.uswatunhasanahtkn.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author User
 */
public class BubbleSortMax {
     int[] getBubbleSortMax(int[] L, int n) {
        int i, k, temp;

        for (i = n - 1; i > 0; i--) {
            for (k = 0; k < i; k++) {
                if (L[k] < L[k + 1]) {
                    temp = L[k];
                    L[k] = L[k + 1];
                    L[k + 1] = temp;
                }
            }
        }
        return L;
    }
    
    public static void main(String[] args) {
        int[] L = {25, 27, 10, 8, 76, 21};
        int i, n = L.length;

        BubbleSortMax app = new BubbleSortMax();
        
        System.out.println("Array Awal");
        System.out.println(Arrays.toString(L));
        app.getBubbleSortMax(L, n);
        System.out.println("\nHasil Pengurutan Array");
        System.out.println(Arrays.toString(L));
    }
}