package id.uswatunhasanahtkn.pertemuan.kesepuluh;

import java.util.Arrays;
/**
 *
 * @author User
 */
public class BubbleSort {
    int[] getBubbleSort(int[] L, int n) {
        int i, k, temp;

        for (i = 0; i < n - 1; i++) {
            for (k = n - 1; k > i; k--) {
                System.out.println("i: " + i + ", k: " + (k - 1) + " --> " + L[k - 1]);
                if (L[k] < L[k - 1]) {
                    temp = L[k];
                    L[k] = L[k - 1];
                    L[k - 1] = temp;
                }
            }
        }
        return L;
    }

    public static void main(String[] args) {
        int[] L = {25, 27, 10, 8, 76, 21};
        int i, n = L.length;

        BubbleSort app = new BubbleSort();
        
        System.out.println("Array Awal");
        System.out.println(Arrays.toString(L));
        app.getBubbleSort(L, n);
        System.out.println("\nHasil Pengurutan Array");
        System.out.println(Arrays.toString(L));
    }
}

