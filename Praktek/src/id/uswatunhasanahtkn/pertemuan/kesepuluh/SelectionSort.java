package id.uswatunhasanahtkn.pertemuan.kesepuluh;

import java.util.Arrays;
/**
 *
 * @author User
 */
public class SelectionSort {
   int[] getSelectionSortMax(int[] L, int n) {
        int i, j, imaks, temp;
        
        for (i = n - 1; i > 0; i--) {
            imaks = 0;
            for (j = 1; j < i + 1; j++) {
                if (L[j] > L[imaks]) {
                    imaks = j;
                }
            }
            temp = L[i];
            L[i] = L[imaks];
            L[imaks] = temp;
        }
        return L;
    }
    int[] getSelectionSortMin(int[] L, int n) {
        int i, j, imin, temp;
        
        for (i = 0; i < n - 1; i++) {
            imin = i;
            
            for (j = i + 1; j < n; j++) {
                if (L[j] < L[imin]) {
                    imin = j;
                }
            }
            temp = L[i];
            L[i] = L[imin];
            L[imin] = temp;
        }
        return L;
    }

    public static void main(String[] args) {
        int[] L = {25, 27, 10, 8, 76, 21};
        int n = L.length;
        SelectionSort app = new SelectionSort();
        
        System.out.println("Array Awal");
        System.out.println(Arrays.toString(L));
        app.getSelectionSortMax(L, n);
        System.out.println("\nHasil Pengurutan Array");
        System.out.println(Arrays.toString(L));
        
//        System.out.println("Array Awal");
//        System.out.println(Arrays.toString(L));
//        app.getSelectionSortMin(L, n);
//        System.out.println("\nHasil Pengurutan Array");
//        System.out.println(Arrays.toString(L));
    }
}
