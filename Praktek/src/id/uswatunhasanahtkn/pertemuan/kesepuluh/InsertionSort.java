package id.uswatunhasanahtkn.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author User
 */
public class InsertionSort {
    int[] getInsertionSort(int[] L, int n) {
        int i, j, y;
        boolean ketemu;
        
        for (i = 1; i < n; i++) {
            y = L[i];
            j = i - 1;
            ketemu = false;
            while ((j >= 0) && (!ketemu)) {
                if (y < L[j]) {
                    L[j + 1] = L[j];
                    j = j - 1;
                } else {
                    ketemu = true;
                }
            }
            L[j + 1] = y;
        }
        return L;
    }
    
    public static void main(String[] args) {
        int[] L = {25, 27, 10, 8, 76, 21};
        int n = L.length;
        InsertionSort app = new InsertionSort();
        
        System.out.println("Array Awal");
        System.out.println(Arrays.toString(L));
        app.getInsertionSort(L, n);
        System.out.println("\nHasil Pengurutan Array");
        System.out.println(Arrays.toString(L));
    }
}
